<?php

namespace App\Providers;

use App\View\Components\EditDeleteButtons;
use App\View\Components\ListItem;
use App\View\Components\Status;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('list-item', ListItem::class);
        Blade::component('status', Status::class);
        Blade::component('controls', EditDeleteButtons::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\AddContact;
use App\Http\Requests\UpdateContact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        $contacts = Contact::all();
        return view('contacts.index', compact('contacts'));
    }

    public function create()
    {
        return view('contacts.create');
    }

    public function store(AddContact $request)
    {
        Contact::updateOrCreate(
            [
                'number'=>$request->number
            ],
            [
                'name'=>$request->name,
                'last_name'=>$request->last_name
            ]);
        return redirect()->route('index');
    }

    public function edit(Contact $contact)
    {
        return view('contacts.edit', compact('contact'));
    }

    public function update(UpdateContact $request, Contact $contact)
    {
        $contact->number = $request->number;
        $contact->name = $request->name;
        $contact->last_name = $request->last_name;
        $contact->save();
        return redirect()->route('index');
    }

    public function approveDelete(Contact $contact){
        return view('contacts.delete', compact('contact'));
    }
    public function destroy(Contact $contact)
    {
        $contact_deleted = $contact->delete();
        return redirect()->route('index');
    }

}

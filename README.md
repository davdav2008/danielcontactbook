Please do

1.  ` composer install`
2.  ` cp .env.example .env`
3.  `php artisan key:generate`
4.  create 'laravel' database (or specify yours in .env)
5.  `php artisan migrate`
6.  `php artisan serve`
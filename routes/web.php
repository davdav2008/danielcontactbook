<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ContactController@index')->name('index');
Route::get('contacts/{contact}/approveDelete', 'ContactController@approveDelete')->name('contacts.approveDelete');
Route::resource('contacts', 'ContactController');


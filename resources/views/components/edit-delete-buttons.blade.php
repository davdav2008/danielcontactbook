<div>
    <input type="hidden" name="contact_id" value="{{$contact->id}}" class="contact_id">
    <ul class="list-inline m-0">
        <li class="list-inline-item">
            <button class="btn btn-primary btn-sm rounded-0" id="edit-item" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
        </li>
        <li class="list-inline-item">
            <button class="btn btn-danger btn-sm rounded-0" id="delete-item" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
        </li>
    </ul>
</div>

<div>
    <li class="list-group-item d-flex justify-content-between align-items-center">
            <div class="col-md-3" style="text-align: center"><span><b>{{$contact->number}}</b></span><br> </div>
            <div class="col-md-3" style="text-align: center"><span>{{$contact->name}}</span></div>
            <div class="col-md-3" style="text-align: center"><span>{{$contact->last_name}}</span></div>
            <div class="col-md-3" style="text-align: center"><x-controls :contact="$contact"/></div>
    </li>
</div>

<form id="edit-form" class="form-horizontal" method="POST" action="{{route('contacts.update', $contact->id)}}">
    @method('PUT')
    @csrf
    <div class="card text-white bg-dark mb-0">
        <div class="card-header">
            <h2 class="m-0">Edit</h2>
        </div>
        <div class="card-body">
            <!-- id -->
            <div class="form-group">
                <label class="col-form-label" for="id">Number</label>
                <input type="text" name="number" class="form-control" id="number" value="{{$contact->number}}" required>
            </div>
            <div class="form-group">
                <label class="col-form-label" for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" value="{{$contact->name}}" required autofocus>
            </div>
            <div class="form-group">
                <label class="col-form-label" for="last-name">Last Name</label>
                <input type="text" name="last_name" class="form-control" id="last_name" value="{{$contact->last_name}}" required autofocus>
            </div>
            <!-- /description -->
        </div>
        <input type="submit" class="btn btn-primary">
        <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
</form>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />
    <title>Daniel ContactBook</title>
</head>
<body>
<div class="container">

<div class="row">
        <button type="button" id="add-item" class="btn btn-primary btn-lg btn-block">ADD</button>
<div class="col-sm-12">
<ul class="list-group">
    @foreach($contacts as $contact)
        <x-list-item :contact="$contact"/>
    @endforeach
</ul>
</div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true" style="width: 30%;height: 100%;margin: auto;margin-top: 2%">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" id="attachment-body-content">
            </div>
        </div>
    </div>
</div>
</body>
<script
        src="https://code.jquery.com/jquery-3.5.0.min.js"
        integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
        crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $(document).on('click', "#add-item", function() {
            $.ajax({
                type: "GET",
                url: "{{route('contacts.create')}}",
            }).done(function(form) {
                $('#attachment-body-content').html(form);
                $('#modal').modal();
            });
        })
        $(document).on('click', "#edit-item", function() {
            var contact = $(this).parent().parent().parent().find('.contact_id').val();
            var url = '{{route('contacts.edit', ":id")}}';
            url = url.replace(':id',contact);
            $.ajax({
                type: "GET",
                url: url,
            }).done(function(form) {
                $('#attachment-body-content').html(form);
                $('#modal').modal();
            });
        })
        $(document).on('click', "#delete-item", function() {
            var contact = $(this).parent().parent().parent().find('.contact_id').val();
            var url = '{{route('contacts.approveDelete', ":id")}}';
            url = url.replace(':id',contact);
            $.ajax({
                type: "GET",
                url: url,
            }).done(function(form) {
                $('#attachment-body-content').html(form);
                $('#modal').modal();

            });
        })
    })
</script>
</html>

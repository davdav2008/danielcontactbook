<form id="sure-form" class="form-horizontal" method="POST" action="{{route('contacts.destroy', $contact)}}">
    @method('DELETE')
    @csrf
    <div class="card text-white bg-dark mb-0">
        <div class="card-body">
            Are you sure you want to delete this contact?
        </div>
        <input type="submit" class="btn btn-primary">
        <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
</form>
